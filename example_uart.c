#include <stdint.h>
#include <stdio.h>
#include "delay.h"
#include "putchar.h"


void init() {
  CLK_CKDIVR = 0x00;   // Set the frequency to 16 MHz
  CLK_PCKENR1 = 0xFF;  // Enable peripherals

  UART1_CR2 = UART_CR2_TEN;                         // Allow TX and RX
  UART1_CR3 &= ~(UART_CR3_STOP1 | UART_CR3_STOP2);  // 1 stop bit
  UART1_BRR2 = 0x03;
  UART1_BRR1 = 0x68;  // 9600 baud
}

int main(){
  uint8_t i = 0xff;
  init();

  while(i--) {
	printf("Hello putchar!");
  }
  delay(100);
  return 15;
}

