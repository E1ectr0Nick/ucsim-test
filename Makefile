CC=sdcc
LD=sdld
CFLAGS+=-mstm8 --model-medium -l stm8
TARGET=example_firmware.ihx

SRC=$(wildcard *.c)
REL=$(SRC:.c=.rel)

all: build

#linking:
build: $(REL)
	$(CC) $(CFLAGS) -o $(TARGET) $^

#compile:
%.rel: %.c
	$(CC) $(CFLAGS) -c $<

test:
#stop if already exist
	-@echo k | ncat localhost 10001 &> /dev/null
	@./test.us > /dev/null &
	@sleep 1 && echo r | ncat localhost 10001 > /dev/null &
#replace to `telnet` if you need input
	@sleep 0.1 && ncat localhost 10000

clean:
	@$(RM) $(TARGET) *.asm *.rel *.lst *.lk *.map *.ihx *.sym *.rst

