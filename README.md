# Build

| Purpose | Command     |
|---------|-------------|
| Build   | `make`      |
| Test    | `make test` |
| Run     | `./test.us` |

See also:
https://count-zero.ru/2018/stm8_uart_adc_i2c/
https://github.com/rumpeltux/stm8s-sdcc-examples/


